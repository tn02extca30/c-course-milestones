1.double pow(double x, int n) - Calculates and returns x^n
 
code:
#include<stdio.h>
double calculatepower(double x,int n);
int main()
{
 double x,pw;
 int n;
 printf("ENTER BASE VALUE - ");
 scanf("%lf",&x);
 printf("ENTER POWER VALUE - ");
 scanf("%d",&n);
 pw=calculatepower(x,n);
printf("%lf",pw);
 return 0;
 }

double calculatepower(double x,int n)
{
 double p=1;
 int o;
 for(o=1;o<=n;o++)
  {
   p=p*x;
  } 
 
 return p;
} 

output:
ENTER BASE VALUE - 2
ENTER POWER VALUE - 3
8.000000

------------------------------------------------------------------------------------------------------------------
2.int gcd(int a, int b) - Calculates and returns the Greatest Common divisor of a and b

code:
#include<stdio.h>
 int calculategcd(int a,int b);
 
 int main()
 {
  int a,b,c;
  printf("ENTER 1ST NUMBER- ");
  scanf("%d",&a);
  printf("ENTER 2ND NUMBER- ");
  scanf("%d",&b);
  c=calculategcd(a,b);
  printf("G.C.D OF %d AND %d IS %d",a,b,c);
  return 0;
 }
  
  int calculategcd(int a,int b)
  {  
  int i,gcd;  
  for(i=2; i<=a && i<=b ;++i)
    {
        if(a%i==0 && b%i==0)
         {
            gcd = i;
         }
    }

    return gcd;
  }

output:
ENTER 1ST NUMBER- 20
ENTER 2ND NUMBER- 25
G.C.D OF 20 AND 25 IS 5

-------------------------------------------------------------------------------------------------------------------
3.int is_prime(unsigned int x) - Returns 1 if x is a prime number and returns 0 if it is not

code:
#include<stdio.h>
 unsigned int findprimeno(unsigned int x);

int main()
{
 unsigned int x;
 int p;
 printf("ENTER ANY NUMBER -");
 scanf("%u",&x);
 p=findprimeno(x);
 printf("%u",p);
 return 0;
}

 unsigned int findprimeno(unsigned int x)
{
 int i; 
 for(i=2;i<x;i++)
 {
  if(x%i==0)
  {
   return 0;
  }
  else
  {
   return 1;
  }
 } 
 return 0;
}

output:
ENTER ANY NUMBER -5
1

----------------------------------------------------------------------------------------------------------------------------------------------------------
4.double FV(double rate, unsigned int nperiods, double PV) - Calculates and returns the Future Value of an investment based on the compound interest formula FV = PV * (1+rate)nperiods

code:
#include<stdio.h>
#include<math.h>
double calculatefv(double pv,double rate,unsigned int nperiods);

int main()
{
 double rate,pv,fv;
 unsigned int nperiods;
 printf("ENTER PRESENT VALUE - ");
 scanf("%lf",&pv);
 printf("ENTER RATE VALUE - ");
 scanf("%lf",&rate);
 printf("ENTER TIME PERIOD - ");
 scanf("%u",&nperiods);
 fv=calculatefv( pv,rate,nperiods);
 printf("THE FUTURE VALUE IS %lf ",fv);
 return 0;
}

double calculatefv(double pv,double rate,unsigned int nperiods)
{
 double f;
 f=pv*(pow(1+rate,nperiods));
 return f;
}

output:
ENTER PRESENT VALUE - 200
ENTER RATE VALUE - 5
ENTER TIME PERIOD - 2
THE FUTURE VALUE IS 7200.000000

---------------------------------------------------------------------------------------------------------------------------------------------
5.double PV(double rate, unsigned int nperiods, double FV) - Calculates and returns the Present Value of an investment based on the compound interest formula FV = PV * (1+rate)nperiods

code:
#include<stdio.h>
#include<math.h>
double calculatepv(double fv,double rate,unsigned int nperiods);

int main()
{
 double rate,fv,pv;
 unsigned int nperiods;
 printf("ENTER FUTURE VALUE - ");
 scanf("%lf",&fv);
 printf("ENTER RATE VALUE - ");
 scanf("%lf",&rate);
 printf("ENTER TIME PERIOD - ");
 scanf("%u",&nperiods);
 pv=calculatepv(fv,rate,nperiods);
 printf("THE PRESENT VALUE IS %lf ",pv);
 return 0;
}

double calculatepv(double fv,double rate,unsigned int nperiods)
{
 double p;
 p=fv/(pow(1+rate,nperiods));
 return p;
}
  

output:
ENTER FUTURE VALUE - 1000
ENTER RATE VALUE - 3
ENTER TIME PERIOD - 1.
THE PRESENT VALUE IS 250.000000

-------------------------------------------------------------------------------------------------------------------------------
6.Function for finding and printing all factors of a number

code:
#include<stdio.h>

int main()
{   

   int x,i;
   printf("ENTER A NUMBER - ");
   scanf("%d",&x);

    printf("FACTORS OF %d are ",x);    
    for(i=1;i<=x;++i)
    {
        if (x%i==0)
        {
            printf("%d ",i);
        }
    }

    return 0;
}	

output:
ENTER A NUMBER - 30
FACTORS OF 30 are 1 2 3 5 6 10 15 30

-----------------------------------------------------------------------------------------------------------------------------------------------------
7.Function for finding and printing all prime factors of a number

code:
#include<stdio.h>

int main()
{   

   int x,i,p,j;
   printf("ENTER A NUMBER - ");
   scanf("%d",&x);

    printf(" PRIME FACTORS OF %d are - ",x);    
    for (i=2;i<=x;i++)
   	{
     	if(x%i==0)
        {
   			p=1;
			for (j=2;j<=i/2;j++)
			{
				if(i%j == 0)
				{
					p=0;
					break;
				}
			} 
			if(p==1)
			{
				printf("\n %d IS A PRIME FACTOR",i);
			}	          	
		}
   }
 
   return 0;
}	



output:
ENTER A NUMBER - 30
 PRIME FACTORS OF 30 are -
 2 IS A PRIME FACTOR
 3 IS A PRIME FACTOR
 5 IS A PRIME FACTOR

--------------------------------------------------------------------------------------------------------------------------------------
8.A function to check if a number is odd or even

code:
#include<stdio.h>

int main()
{
 int x,y;
 printf("ENTER ANY NUMBER - ");
 scanf("%d",&x);
 y=x%2;
 if(y==0)
 {
  printf("THE %d IS A EVEN NUMBER ",x);
 }
 else 
 {
  printf("THE %d IS A ODD NUMBER ",x);
 }
return 0; 
}

output:
ENTER ANY NUMBER - 8
THE 8 IS A EVEN NUMBER

--------------------------------------------------------------------------------------------------------------------------------------
9.A function to get LCM of 3 numbers

code:
#include<stdio.h>
int main()
{
    int x,y,z,a;
    printf("ENTER 1ST NUMBER - ");
    scanf("%d",&x);
	printf("ENTER 2ND NUMBER - ");
    scanf("%d",&y);
	printf("ENTER 3RD NUMBER - ");
    scanf("%d",&z);

    if(x>y)
	{ 
	  if(x>z)
	  {
	    a=x;
	  }
	  else
	  {
	    a=z;
	  }
    }
    else
	{
	  if(y>z)
	  {
	    a=y;
	  }
      else
      {
        a=z;
      }
	} 
    	
    while(1)
    {
        if(a%x==0 && a%y==0 && a%z==0)
        {
            printf("The LCM of %d ,%d and %d is %d.",x,y,z,a);
            break;
        }
        ++a;
    }
    return 0;
}


output:
ENTER 1ST NUMBER - 40
ENTER 2ND NUMBER - 20
ENTER 3RD NUMBER - 30
The LCM of 40 ,20 and 30 is 120.

--------------------------------------------------------------------------------------------------------------------------------------
10.unsigned int fact(unsigned int n) - Calculates and returns the factorial of n.

code:

#include<stdio.h>
unsigned int calculatefact(unsigned int n);

int main()
{
 unsigned int n;
 printf("ENTER ANY NUMBER - ");
 scanf("%u",&n);
 unsigned int fact=calculatefact( n);
 printf("FACTORIAL OF %u IS - %u ",n,fact);
 return 0;
}
 
unsigned int calculatefact(unsigned int n)
{ 
 int i=1;
 unsigned int f=1;
 while(i<=n)
 {
  f=f*i;
  i++;
 }
 return f;
} 

output:
ENTER ANY NUMBER - 9
FACTORIAL OF 9 IS - 362880

--------------------------------------------------------------------------------------------------------------------------------------
11.nCr - Combinations

code:
#include<stdio.h>
int factorial(int);
int calculatencr(int n,int r);
 
int main()
{
   int n,r;
   printf("ENTER THE VALUE OF N -");
   scanf("%d",&n);
   printf("ENTER THE VALUE OF R -");
   scanf("%d",&r);
   int ncr =calculatencr(n,r);
   printf("THE NCR OF GIVEN DATA IS %d",ncr);
   return 0;
}
 
int calculatencr(int n, int r)
 {
   int a;
   a= factorial(n)/(factorial(r)*factorial(n-r));
   return a;
}
 
int factorial(int n)
{
   int c, a= 1;
   for (c = 1; c <= n; c++)
      a=a*c;
   return a;
}


output:
ENTER THE VALUE OF N -2
ENTER THE VALUE OF R -3
THE NCR OF GIVEN DATA IS 0

--------------------------------------------------------------------------------------------------------------------------------------
12.nPr - Permutations

code:
#include<stdio.h>
int factorial(int);
int calculatenpr(int n,int r);
 
int main()
{
   int n, r;
   printf("ENTER THE VALUE OF N -");
   scanf("%d",&n);
   printf("ENTER THE VALUE OF R -");
   scanf("%d",&r);
   int npr =calculatenpr(n,r);
   printf("THE NPR VALUE OF GIVEN DATA IS %d ", npr);
   return 0;
}
 
 int calculatenpr(int n, int r)
 {
  int a;
 
   a = factorial(n)/factorial(n-r);
 
   return a;
} .
 
int factorial(int n)
 {
   int c,a=1;
   for (c = 1; c <= n; c++)
      a = a*c;
   return a;
}


output:
ENTER THE VALUE OF N -2
ENTER THE VALUE OF R -3
THE NPR VALUE OF GIVEN DATA IS 2

--------------------------------------------------------------------------------------------------------------------------------------
13.void fibonacci(int n) - Prints the first n fibonacci numbers

code:
#include<stdio.h>
#include<conio.h>
int fibonacci (int n);

int main()
{
    int i,n;

    printf("ENTER THE VALUE OF N : ");
    scanf("%d", &n);
	int f = fibonacci (n);
    return 0;
}
	
int fibonacci (int n)
{	
    int i,n1=0,n2=1,nt;
    printf("FIBONACCI SERIES : ");
    for (i=1;i<=n;++i)
    {
        printf("%d, ",n1);
        nt=n1+n2;
        n1=n2;
        n2=nt;
    }
	
}

output:
ENTER THE VALUE OF N : 8
FIBONACCI SERIES : 0, 1, 1, 2, 3, 5, 8, 13,


